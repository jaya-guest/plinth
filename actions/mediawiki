#!/usr/bin/python3
#
# This file is part of FreedomBox.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
"""
Configuration helper for MediaWiki.
"""

import argparse
import os
import re
import subprocess
import sys
import tempfile

from plinth import action_utils
from plinth.utils import generate_password, grep

MAINTENANCE_SCRIPTS_DIR = "/usr/share/mediawiki/maintenance"
CONF_FILE = '/var/lib/mediawiki/LocalSettings.php'


def parse_arguments():
    """Return parsed command line arguments as dictionary."""
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand', help='Sub command')

    subparsers.add_parser('enable', help='Enable MediaWiki')
    subparsers.add_parser('disable', help='Disable MediaWiki')
    subparsers.add_parser('setup', help='Setup MediaWiki')
    subparsers.add_parser('update', help='Run MediaWiki update script')

    help_pub_reg = 'Enable/Disable/Status public user registration.'
    pub_reg = subparsers.add_parser('public-registrations', help=help_pub_reg)
    pub_reg.add_argument('command', choices=('enable', 'disable', 'status'),
                         help=help_pub_reg)

    help_private_mode = 'Enable/Disable/Status private mode.'
    private_mode = subparsers.add_parser('private-mode',
                                         help=help_private_mode)
    private_mode.add_argument('command', choices=('enable', 'disable',
                                                  'status'),
                              help=help_private_mode)

    change_password = subparsers.add_parser('change-password',
                                            help='Change user password')
    change_password.add_argument('--username', default='admin',
                                 help='name of the MediaWiki user')
    change_password.add_argument('--password',
                                 help='new password for the MediaWiki user')

    subparsers.required = True
    return parser.parse_args()


def subcommand_setup(_):
    """Run the installer script to create database and configuration file."""
    data_dir = '/var/lib/mediawiki-db/'
    if not os.path.exists(data_dir):
        os.mkdir(data_dir)

    if not os.path.exists(os.path.join(data_dir, 'my_wiki.sqlite')):
        install_script = os.path.join(MAINTENANCE_SCRIPTS_DIR, 'install.php')
        password = generate_password()
        with tempfile.NamedTemporaryFile() as password_file_handle:
            password_file_handle.write(password.encode())
            subprocess.check_call([
                'php', install_script, '--confpath=/etc/mediawiki',
                '--dbtype=sqlite', '--dbpath=' + data_dir,
                '--scriptpath=/mediawiki', '--passfile',
                password_file_handle.name, 'Wiki', 'admin'
            ])
    subprocess.run(['chmod', '-R', 'o-rwx', data_dir], check=True)
    subprocess.run(['chown', '-R', 'www-data:www-data', data_dir], check=True)
    _disable_public_registrations()
    _disable_anonymous_editing()
    _change_logo()
    _enable_file_uploads()


def _disable_public_registrations():
    """Edit MediaWiki configuration to disable public registrations."""
    if not grep(r'\$wgGroupPermissions.*createaccount', CONF_FILE):
        with open(CONF_FILE, 'a') as file_handle:
            file_handle.write(
                "$wgGroupPermissions['*']['createaccount'] = false;\n")


def _disable_anonymous_editing():
    """Edit MediaWiki configuration to allow anonymous users from editing.

    MediaWiki instances typically get a lot of spam bots.
    """
    if not grep(r'\$wgGroupPermissions.*edit', CONF_FILE):
        with open(CONF_FILE, 'a') as file_handle:
            file_handle.write("$wgGroupPermissions['*']['edit'] = false;\n")


def _change_logo():
    """Change the placeholder logo to MediaWiki's official logo"""
    lines = open(CONF_FILE, 'r').readlines()
    with open(CONF_FILE, 'w') as file_handle:
        for line in lines:
            if re.match('^\s*\$wgLogo', line):
                line = line.replace('assets/wiki.png', 'assets/mediawiki.png')

            file_handle.write(line)


def _enable_file_uploads():
    """Enable file uploads in mediawiki"""
    conf_line = '$wgEnableUploads = true;\n'
    conf_updated = False
    with open(CONF_FILE, 'r') as conf_file:
        lines = conf_file.readlines()
    with open(CONF_FILE, 'w') as conf_file:
        for line in lines:
            if "$wgEnableUploads" in line:
                conf_file.write(conf_line)
                conf_updated = True
            else:
                conf_file.write(line)
        if not conf_updated:
            conf_file.write(conf_line)


def subcommand_change_password(arguments):
    """Change the password for a given user"""
    new_password = ''.join(sys.stdin)
    change_password_script = os.path.join(MAINTENANCE_SCRIPTS_DIR,
                                          'changePassword.php')

    subprocess.check_call([
        'php', change_password_script, '--user', arguments.username,
        '--password', new_password
    ])


def subcommand_update(_):
    """Run update.php maintenance script when version upgrades happen."""
    update_script = os.path.join(MAINTENANCE_SCRIPTS_DIR, 'update.php')
    subprocess.check_call(['php', update_script])


def subcommand_enable(_):
    """Enable web configuration and reload."""
    action_utils.service_enable('mediawiki-jobrunner')
    action_utils.webserver_enable('mediawiki')


def subcommand_disable(_):
    """Disable web configuration and reload."""
    action_utils.webserver_disable('mediawiki')
    action_utils.service_disable('mediawiki-jobrunner')


def subcommand_public_registrations(arguments):
    """Enable or Disable public registrations for MediaWiki."""

    with open(CONF_FILE, 'r') as conf_file:
        lines = conf_file.readlines()

    def is_pub_reg_line(line):
        return line.startswith("$wgGroupPermissions['*']['createaccount']")

    if arguments.command == 'status':
        conf_lines = list(filter(is_pub_reg_line, lines))
        if conf_lines:
            print('enabled' if 'true' in conf_lines[0] else 'disabled')
        else:
            print('disabled')
    else:
        with open(CONF_FILE, 'w') as conf_file:
            for line in lines:
                if is_pub_reg_line(line):
                    words = line.split()
                    if arguments.command == 'enable':
                        words[-1] = 'true;'
                    else:
                        words[-1] = 'false;'
                    conf_file.write(" ".join(words) + '\n')
                else:
                    conf_file.write(line)


def subcommand_private_mode(arguments):
    """Enable or Disable Private mode for wiki"""
    with open(CONF_FILE, 'r') as conf_file:
        lines = conf_file.readlines()

    def is_edit_line(line):
        return line.startswith("$wgGroupPermissions['*']['edit']")

    def is_read_line(line):
        return line.startswith("$wgGroupPermissions['*']['read']")

    edit_conf_lines = list(filter(is_edit_line, lines))
    read_conf_lines = list(filter(is_read_line, lines))
    if arguments.command == 'status':
        if edit_conf_lines and read_conf_lines:
            print('enabled' if ('false' in read_conf_lines[0]) and (
                'false' in edit_conf_lines[0]) else 'disabled')
        else:
            print('disabled')
    else:
        with open(CONF_FILE, 'w') as conf_file:
            conf_value = 'false;' if arguments.command == 'enable' else 'true;'
            for line in lines:
                if is_edit_line(line) or is_read_line(line):
                    words = line.split()
                    words[-1] = conf_value
                    conf_file.write(" ".join(words) + '\n')
                else:
                    conf_file.write(line)

            if not edit_conf_lines:
                conf_file.write("$wgGroupPermissions['*']['edit'] = " +
                                conf_value + '\n')
            if not read_conf_lines:
                conf_file.write("$wgGroupPermissions['*']['read'] = " +
                                conf_value + '\n')


def main():
    """Parse arguments and perform all duties."""
    arguments = parse_arguments()

    subcommand = arguments.subcommand.replace('-', '_')
    subcommand_method = globals()['subcommand_' + subcommand]
    subcommand_method(arguments)


if __name__ == '__main__':
    main()
